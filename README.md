# HexAI

Una Inteligencia Artifical Simple que juega Hex.

## Equipo
* Alejandro Salgado Gómez [asalgad2@eafit.edu.co](mailto:asalgad2@eafit.edu.co)
* Luis Miguel Mejía Suárez [lmejias3@eafit.edu.co](mailto:lmejias3@eafit.edu.co)

## Uso Y Dependencias
### Dependencias
HexAI necesita de [Python 3](https://docs.python.org/3/) y [NumPy](http://www.numpy.org/) para su funcionamiento

Para instalar NumPy en su sistema, ejecutar el siguiente comando; puede requerir permisos de administrador

    pip3 install numpy

### Uso
Para hacer uso de HexAI en su programa, debe de copiar el archivo agente\_alejandro\_luis.py
a la carpeta donde se encuentra su programa.

Luego debe de importar el agente, instanciarlo y ejecutar el metodo _"play"_ continuamente.

A continuación un ejemplo

```python
from agente_alejandro_luis import agente_alejandro_luis
#El constructor del agente recibe como parametro cuál jugador es (1 o 2)
agente = agente_alejandro_luis(1)
#El método play recibe una matrix de 11 * 11 que representa el tablero del juego en el momento actual
#y retorna una tupla (y,x) que representa la jugada a realizar
jugada1 = agente.play(estado)
#El método play puede ser llamado con dos parametros, pero el segudo siempre es ignorado.
#Este parametro existe por temas de compatibilidad
jugada2 = agente.play(estado, 2)
```

**_Notas:_**
* Llamar al constructor del agente con un valor **diferente a _1 o 2_** para el jugador generará un [ValueError](https://docs.python.org/3.5/library/exceptions.html#ValueError)
* Llamar al método play con una matrix de un tamaño **diferente a _11 \* 11_** para el estado generaŕa un [ValueError](https://docs.python.org/3.5/library/exceptions.html#ValueError)
* El método play espera que entre llamada y llamada recibir la misma matrix con **dos posiciones diferentes**
una es la jugada que el retorno en la llamada anterior, la segunda es la jugada realizada por el rival.
No respetar esta regla hará que el agente no responda de manera adecuada e inclusive puede generar errores en el programa.

## Algoritmos
HexAI juega hex usando movimientos por reflejo en ciertas jugadas donde se conocen la respuesta optima para el estado dado.
En caso de no conocer una respuesta para el estado actual, el agente dará ejecución a **Mini-Max con _$`\alpha - \beta`$ pruning_** de manera iterativa.

Los algoritmos que utiliza HexAI fueron construidos usando como base el concepto de **conexiones virtuales** _(Vadim, Anshelevich. 2002)_

En este **[Vídeo](https://www.youtube.com/watch?v=SpeeroH3X7k)** se da una explicación detallada de los algoritmos implementados.

## Bibliografía
* Vadim, Anshelevich. (2002). A hierarchical approach to computer Hex. Recuperado de: http://vanshel.com/Hexy/Publications/VAnshelevich-ARTINT.pdf
* Rijswijck, Jack. (2002). Search and evaluation in Hex. Recuperado de: http://www.cs.cornell.edu/~adith/docs/y_hex.pdf