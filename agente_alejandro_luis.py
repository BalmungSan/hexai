#imports
from collections import defaultdict
import itertools
import numpy as np
import signal

#HexAI agent
#An artifical intelligent agent for playing hex based on the concept of virtual
#connections
#The agent has built-in reflex movements and a iterative mini-max search tree
#with alpha-beta pruning
#This agent implements the shortest path in a graph as its evaluation function
class agente_alejandro_luis:
  #constructor, takes if the agent is player one or two
  def __init__(self, player):
    #check that player is 1 or 2
    if (player != 1 and player != 2): 
      raise ValueError("Player must be 1 or 2")

    #game info
    self.state    = np.zeros((11,11), int)
    self.player   = player
    self.opponent = 2 if player == 1 else 1
    self.turn     = 0 if player == 1 else 1
    self.empty    = 0

    #board function
    self.bBoard = np.vectorize(lambda x: 1.1 if x == 0 else 0.0 if x == 1 else 1000.0)
    self.wBoard = np.vectorize(lambda x: 1.1 if x == 0 else 0.0 if x == 2 else 1000.0)

  #check if a virtual connection was attacked
  def check(self, rpoint):
    y,x = rpoint

    #borders
    if (y == 0 or x == 0 or y == 10 or x == 10):
      if (self.player == 1):
        #black
        #upper border
        if (y == 0 and x != 10 and self.state[y+1,x] == self.player and self.state[y,x+1] == self.empty):
          return (y,x+1)
        if (y == 0 and x != 0 and self.state[y+1,x-1] == self.player and self.state[y,x-1] == self.empty):
          return (y,x-1)
        #lower border
        if (y == 10 and x != 10 and self.state[y-1,x+1] == self.player and self.state[y,x+1] == self.empty):
          return (y,x+1)
        if (y == 10 and x != 0 and self.state[y-1,x] == self.player and self.state[y,x-1] == self.empty):
          return (y,x-1)
        return None
      elif (self.player == 2):
        #white
        #left border
        if (x == 0 and y != 10 and self.state[y,x+1] == self.player and self.state[y+1,x] == self.empty):
          return (y+1,x)
        if (x == 0 and y != 0 and self.state[y-1,x+1] == self.player and self.state[y-1,x] == self.empty):
          return (y-1,x)
        #right border
        if (x == 10 and y != 10 and self.state[y+1,x-1] == self.player and self.state[y+1,x] == self.empty):
          return (y+1,x)
        if (x == 10 and y != 0 and self.state[y,x-1] == self.player and self.state[y-1,x] == self.empty):
          return (y-1,x)
        return None
      else:
        return None

    #A
    if (self.state[y+1,x] == self.player and self.state[y,x-1] == self.player and self.state[y+1,x-1] == self.empty):
      return (y+1,x-1)
    if (self.state[y+1,x] == self.player and self.state[y-1,x+1] == self.player and self.state[y,x+1] == self.empty):
      return (y,x+1)

    #B
    if (self.state[y+1,x-1] == self.player and self.state[y-1,x] == self.player and self.state[y,x-1] == self.empty):
      return (y,x-1)
    if (self.state[y+1,x-1] == self.player and self.state[y,x+1] == self.player and self.state[y+1,x] == self.empty):
      return (y+1,x)

    #C
    if (self.state[y,x-1] == self.player and self.state[y-1,x+1] == self.player and self.state[y-1,x] == self.empty):
      return (y-1,x)
    if (self.state[y,x-1] == self.player and self.state[y+1,x] == self.player and self.state[y+1,x-1] == self.empty):
      return (y+1,x-1)

    #D
    if (self.state[y-1,x] == self.player and self.state[y,x+1] == self.player and self.state[y-1,x+1] == self.empty):
      return (y-1,x+1)
    if (self.state[y-1,x] == self.player and self.state[y+1,x-1] == self.player and self.state[y,x-1] == self.empty):
      return (y,x-1)

    #E
    if (self.state[y-1,x+1] == self.player and self.state[y+1,x] == self.player and self.state[y,x+1] == self.empty):
      return (y,x+1)
    if (self.state[y-1,x+1] == self.player and self.state[y,x-1] == self.player and self.state[y-1,x] == self.empty):
      return (y-1,x)

    #F
    if (self.state[y,x+1] == self.player and self.state[y+1,x-1] == self.player and self.state[y+1,x] == self.empty):
      return (y+1,x)
    if (self.state[y,x+1] == self.player and self.state[y-1,x] == self.player and self.state[y-1,x+1] == self.empty):
      return (y-1,x+1)

    #None
    else:
      return None

  #evaluation function
  def eval(self):
    #evaluate a hex graph
    def evalGraph(graph):
      #table to store intermediate results and avoid innecesary computations
      minTable = defaultdict(lambda: None)
      minTable[(12,12)] = 0

      #return the minimum path for a node, recursive function
      def evalNode(node):
        #check if the minimum path for this node was already computed
        if minTable[node] is not None:
          return minTable[node]

        #if not, calculate the min
        nodes = graph[node]
        m = min(map(lambda nv: evalNode(nv[0]) + nv[1], nodes))
        minTable[node] = m
        return m

      #return the minimum path from the first node
      return evalNode((-1,-1))

    #create a directed graph from a hex board
    def toGraph(board, player):
      graph = {}
      for y, x in itertools.product(range(11), range(11)):
        nodes = set()
        if (player == 'b'):
          #black -> lower left
          if (x != 0 and y != 10):
            nodes.add(((y+1,x-1), board[y,x] + board[y+1,x-1] + 0.1))
        else:
          #white -> upper left
          if (x != 10 and y != 0): 
            nodes.add(((y-1,x+1), board[y,x] + board[y-1,x+1] + 0.1))
        #lower
        if (y != 10):
          nodes.add(((y+1,x), board[y,x] + board[y+1,x] + 0.1))
        #right
        if (x != 10):
          nodes.add(((y,x+1), board[y,x] + board[y,x+1] + 0.1))

        #virtual connections
        if (board[y,x] == 0):
          if (player == 'b'):
            #black -> lower left
            if (y < 10  and x > 1 and board[y+1,x-2] == 0 and board[y,x-1] == 1 and board[y+1,x-1] == 1):
              nodes.add(((y+1,x-2), 0.5))
          else:
            #white -> upper left
            if (y < 1  and x > 10 and board[y-1,x+2] == 0 and board[y,x+1] == 1 and board[y-1,x+1] == 1):
              nodes.add(((y-1,x+2), 0.5))
          #lower
          if (y < 9 and x > 0 and board[y+2,x-1] == 0 and board[y+1,x] == 1 and board[y+1,x-1] == 1):
            nodes.add(((y+2,x-1), 0.5))
          #lower right
          if (y != 10 and x != 10 and board[y+1,x+1] == 0 and board[y+1,x] == 1 and board[y,x+1] == 1):
            nodes.add(((y+1,x+1), 0.5))

        graph[(y,x)] = nodes
        graph[(y,x)] = nodes

      #add s (-1,-1) and t (12,12) nodes
      if (player == 'b'):
        #black
        graph[(-1,-1)] = set(((0,x), 1.0) for x in range(11))
        for x in range(11):
          graph[10,x].add(((12,12), 1.0))
      else:
        #white
        graph[(-1,-1)] = set(((x,0), 1.0) for x in range(11))
        for x in range(11):
          graph[x,10].add(((12,12), 1.0))

      return graph

    #generate the graphs
    bGraph = toGraph(self.bBoard(self.state), 'b')
    wGraph = toGraph(self.wBoard(self.state), 'w')

    #evaluate each graph
    bval = evalGraph(bGraph)
    wval = evalGraph(wGraph)

    #return the value for this state
    return (wval/bval) if self.player == 1 else (bval/wval)

  #get the position played by opponent in the last turn
  def getPlayed(self, newstate):
    yIndex, xIndex = np.where(self.state != newstate)
    return (yIndex[0], xIndex[0])

  def setVc(self, i, j):
    vc = []
    vc.append((i-1,j-1))
    vc.append((i-2,j+1))
    vc.append((i-1,j+2))
    vc.append((i+1,j+1))
    vc.append((i+2,j-1))
    vc.append((i+1,j-2))
    return vc

  def getVc(self, i, j):
    vc = self.setVc(i,j)
    for pos in range(6):
      if not self.evalVc(pos, i,j):
        vc[pos] = None
    return vc

  def evalVc(self, case, i, j):
    if case == 0:
      vci, ci1, ci2 = i-1, i, i-1
      vcj, cj1, cj2 = j-1, j-1, j
    elif case == 1:
      vci, ci1, ci2 = i-2, i-1, i-1
      vcj, cj1, cj2 = j+1, j, j+1
    elif case == 2:
      vci, ci1, ci2 = i-1, i-1, i
      vcj, cj1, cj2 = j+2, j+1, j+1
    elif case == 3:
      vci, ci1, ci2 = i+1, i, i+1
      vcj, cj1, cj2 = j+1, j+1, j
    elif case == 4:
      vci, ci1, ci2 = i+2, i+1, i+1
      vcj, cj1, cj2 = j-1, j, j-1
    else:
      vci, ci1, ci2 = i+1, i+1, i
      vcj, cj1, cj2 = j-2, j-1, j-1

    return self.verfiyVcPositions(vci, vcj, ci1, cj1, ci2, cj2)

  def verfiyVcPositions(self, vci, vcj, ci1, cj1, ci2, cj2):
    if vci < 0 or vci > 10:
      return False
    if vcj < 0 or vcj > 10:
      return False

    if self.state[vci][vcj] != self.empty:
      return False

    if self.state[ci1][cj1] != self.empty or self.state[ci2][cj2] != self.empty:
      return False

    return True

  def setNb(self, i, j):
    neighbors = []
    neighbors.append((i-1, j))
    neighbors.append((i-1, j+1))
    neighbors.append((i+1, j))
    neighbors.append((i+1, j-1))
    return neighbors

  def getNb(self, i, j):
    nb = self.setNb(i,j)
    for pos in range(4):
      if not self.evalNb(pos, i,j):
        nb[pos] = None
    return nb

  def evalNb(self, case, i, j):
    if case == 0:
      nbi = i-1
      nbj = j
    elif case == 1:
      nbi = i-1
      nbj = j+1
    elif case == 2:
      nbi = i+1
      nbj = j
    elif case == 3:
      nbi = i+1
      nbj = j-1

    return self.verfiyNbPositions(nbi, nbj)

  def verfiyNbPositions(self, nbi, nbj):
    if nbi < 0 or nbi > 10:
      return False
    if nbj < 0 or nbj > 10:
      return False

    if self.state[nbi][nbj] != self.empty:
      return False
    return True

  #get a random empty child
  def getRandomChild(self):
    yIndexes, xIndexes = np.where(self.state == self.empty)
    emptyNodes = np.stack((yIndexes, xIndexes),axis=1)
    ran = emptyNodes[np.random.choice(len(emptyNodes))]
    return(ran[0], ran[1])

  def getSucc(self,plyr):
    successors = set()
    for i in range(11):
      for j in range(11):
        if self.state[i][j] == plyr:
         for succ in self.getVc(i,j):
           if succ:
             successors.add(succ)
         for adj in self.getNb(i,j):
           if adj:
             successors.add(adj)   
    if (plyr == self.opponent):
      successors.add(self.getRandomChild())
    return successors

  def getOpponent(self, plyr):
    if plyr == self.player:
      return self.opponent
    else:
      return self.player

  def alphaBeta(self,  plyr, i=0, j=0, alpha=0, beta=1000, depth=2, maxi=True, frst=True):
    if depth == 0:
      return [self.eval(), [i,j]]

    successors = self.getSucc(plyr)

    if not successors:
      return [self.eval(), [i,j]]

    opponent = self.getOpponent(plyr)

    node_val = [None, [i,j]]
    for i2, j2 in successors:
      self.state[i2][j2] = plyr
      child_val = self.alphaBeta(opponent, i2, j2, alpha, beta, depth-1, not maxi, frst=False)
      self.state[i2][j2] = self.empty

      if maxi and child_val[0] <= beta:
        intermediate = self.select("max", node_val[0], child_val[0])
        alpha = intermediate
        if frst and intermediate != node_val[0]:
          node_val[1] = [i2,j2]
        node_val[0] = intermediate
      elif not maxi and child_val[0] >= alpha:
        node_val[0] = self.select("min", node_val[0], child_val[0])
        beta = node_val[0]
      else:
        if node_val[0] is None:
          node_val[0] = child_val[0]
        break

    return node_val

  def select(self, action, actual, new):
    if actual is None:
        return new

    if action == "max":
      if actual > new:
        return actual
    elif action == "min":
      if actual < new:
        return actual
    return new

  #main method
  def play(self, newstate, p = None):
    #check that state is of size 11 * 11
    newstate = np.array(newstate)
    if (newstate.shape != (11,11)):
      raise ValueError("State must be of size 11 * 11")

    #alarm in five seconds
    def handler(signum, frame):
      raise RuntimeError()
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(5)

    #first move is a reflex
    if (self.turn == 0):
      signal.alarm(0)
      self.turn += 2
      self.state[5,5] = self.player
      return (5,5)
    elif (self.turn == 1):
      #get the opponent movement
      rpoint = self.getPlayed(newstate)
      self.state[rpoint] = self.opponent
      signal.alarm(0)
      self.turn += 2
      if (self.state[5,5] == 0):
        self.state[5,5] = self.player
        return (5,5)
      else:
        self.state[7,4] = self.player
        return (7,4)

    #get the opponent movement
    rpoint = self.getPlayed(newstate)
    self.state[rpoint] = self.opponent

    #check if a virtual connection was compromised
    c = self.check(rpoint)
    if c is not None:
      signal.alarm(0)
      self.turn += 2
      self.state[c] = self.player
      return c

    try:
      #run mini-max with alpha-beta pruning until the time runs out
      ans = self.getRandomChild()
      i = 2
      stateCopy = np.copy(self.state)
      while True:
        weight, ans = self.alphaBeta(self.player, depth=i)
        i += 1
    except RuntimeError:
      self.state = stateCopy
      i,j = ans 
      self.turn += 2
      self.state[i][j] = self.player
      return (i,j)
